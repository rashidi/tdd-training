package com.gfk.training.v2.service;

/**
 * @author Rashidi Zin
 */
public class CalculateServiceTest {

    public static void main(String args[]) {
        CalculateService service = new CalculateService();
        assertEqual(0, service.getResult());

        service.setFirstNumber(10);
        assertEqual(10, service.getResult());

        service.setOperator("+");
        service.setSecondNumber(20);
        assertEqual(30, service.getResult());

        service.setOperator("-");
        service.setSecondNumber(20);
        assertEqual(10, service.getResult());

        service.setSecondNumber(25);
        assertEqual(10, service.getResult());

        try {
            service.setOperator("*");
        } catch (IllegalArgumentException e) {
            if (!"Unsupported operation".equals(e.getMessage())) {
                throw new AssertionError("Expected error is: 'Unsupported operation' but instead " + e.getMessage());
            }
        }

        service.setSecondNumber(10);
        assertEqual(10, service.getResult());
    }

    private static void assertEqual(int expected, int actual) {
        if (expected != actual) {
            throw new AssertionError("Expected result is " + expected + " but instead " + actual);
        }
    }
}
