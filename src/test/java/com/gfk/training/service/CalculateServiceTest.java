package com.gfk.training.service;

/**
 * @author Rashidi Zin
 */
public class CalculateServiceTest {

    public static void main(String[] args) {
        CalculateService service = new CalculateService(10, 12, "+");

        if (22 != service.calculate()) {
            throw new AssertionError("Expected value is 22");
        }

        service = new CalculateService(20, 10, "-");

        if (10 != service.calculate()) {
            throw new AssertionError("Expected value is 10");
        }
    }
}
