package com.gfk.training.junit;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.math.BigDecimal;

import static java.math.BigDecimal.ZERO;
import static org.junit.Assert.assertEquals;
import static org.junit.rules.ExpectedException.none;

/**
 * @author Rashidi Zin
 */
@RunWith(JUnit4.class)
public class AccountServiceTest {

    @Rule
    public ExpectedException expect = none();

    private AccountService service;

    @Before
    public void init() {
        service = new AccountService();
    }

    @Test
    public void initialValueIsZero() {
        assertEquals(ZERO, service.getBalance());
    }

    @Test
    public void deposit() {
        final BigDecimal amount = new BigDecimal(100);

        service.deposit(amount);

        assertEquals(amount, service.getBalance());
    }

    @Test
    public void depositValueMustBeMoreThanZero() {
        expect.expect(IllegalArgumentException.class);
        expect.expectMessage("Deposit amount must be more than zero!");

        service.deposit(new BigDecimal(-100));
    }

    @Test
    public void withdraw() {
        final BigDecimal depositAmount = new BigDecimal(110);
        final BigDecimal withdrawalAmount = new BigDecimal(10);

        service.deposit(depositAmount);
        service.withdraw(withdrawalAmount);

        assertEquals(new BigDecimal(100), service.getBalance());
    }

    @Test
    public void withdrawWithInvalidAmount() {
        expect.expect(IllegalArgumentException.class);
        expect.expectMessage("Invalid withdrawal amount!");

        service.deposit(new BigDecimal(100));
        service.withdraw(ZERO);
    }

    @Test
    public void withdrawMoreThanBalance() {
        expect.expect(IllegalArgumentException.class);
        expect.expectMessage("Withdrawal amount must not be more than balance!");

        service.deposit(new BigDecimal(100));
        service.withdraw(new BigDecimal(200));
    }

    @Test
    public void withdraw10WithBalance100() {
        expect.expect(IllegalArgumentException.class);
        expect.expectMessage("Balance must be minimum of 100");

        service.deposit(new BigDecimal(100));
        service.withdraw(new BigDecimal(10));
    }

    @Test
    public void withdraw20WithBalance200() {
        service.deposit(new BigDecimal(200));
        service.withdraw(new BigDecimal(20));

        assertEquals(new BigDecimal(180), service.getBalance());
    }

    @Test
    public void withdraw50WithBalance110() {
        expect.expect(IllegalArgumentException.class);
        expect.expectMessage("Balance must be minimum of 100");

        service.deposit(new BigDecimal(110));
        service.withdraw(new BigDecimal(50));
    }

    @Test
    public void openAccountWith50() {
        final BigDecimal amount = new BigDecimal(50);

        service = new AccountService(amount);

        assertEquals(amount, service.getBalance());
    }

    @Test
    public void openAccountWithNegativeFigure() {
        expect.expect(IllegalArgumentException.class);
        expect.expectMessage("Invalid amount!");

        new AccountService(new BigDecimal(-1));
    }

    @Test
    public void transfer50WithBalance110(){
        expect.expect(IllegalArgumentException.class);
        expect.expectMessage("Balance must be minimum of 100");

        service.deposit(new BigDecimal(110));
        service.transfer(new BigDecimal(50));
    }

    @Test
    public void transfer400WithBalance200() {
        expect.expect(IllegalArgumentException.class);
        expect.expectMessage("Insufficient balance to transfer");

        service = new AccountService(new BigDecimal(200));
        service.transfer(new BigDecimal(400));
    }

    @Test
    public void transfer20WithBalance200() {
        service = new AccountService(new BigDecimal(200));
        service.transfer(new BigDecimal(20));

        assertEquals(new BigDecimal(180), service.getBalance());
    }

    @Test
    public void transferWithZero() {
        expect.expect(IllegalArgumentException.class);
        expect.expectMessage("Invalid transfer amount");

        service.transfer(ZERO);
    }

    @Test
    public void transferWithNegativeAmount() {
        expect.expect(IllegalArgumentException.class);
        expect.expectMessage("Invalid transfer amount");

        service.transfer(new BigDecimal(-100));
    }
}
