package com.gfk.training.domain;

/**
 * @author Rashidi Zin
 */
public enum CalculateEnum {
    ADD("+"),
    SUBTRACT("-")
    ;

    private String canonicalValue;

    CalculateEnum(String canonicalValue) {
        this.canonicalValue = canonicalValue;
    }

    public String getCanonicalValue() {
        return canonicalValue;
    }

    public static CalculateEnum fromCanonicalValue(String canonicalValue) {

        switch (canonicalValue) {
            case "+":
                return CalculateEnum.ADD;
            case "-":
                return CalculateEnum.SUBTRACT;
            default:
                return null;
        }
    }
}
