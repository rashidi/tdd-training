package com.gfk.training.v2.service;

/**
 * @author Rashidi Zin
 */
public class CalculateService {

    private int firstNumber;
    private String operator;
    private int secondNumber;

    public CalculateService() {
        firstNumber = 0;
    }

    public int getResult() {
        if ("+".equals(this.operator)) {
            firstNumber += secondNumber;
        }
        else if ("-".equals(this.operator)) {
            firstNumber -= secondNumber;
        }

        this.operator = null;
        return firstNumber;
    }

    public void setFirstNumber(int firstNumber) {
        this.firstNumber = firstNumber;
    }

    public void setOperator(String operator) {
        if (!"+".equals(operator) && !"-".equals(operator)) {
            throw new IllegalArgumentException("Unsupported operation");
        }

        this.operator = operator;
    }

    public void setSecondNumber(int secondNumber) {
        this.secondNumber = secondNumber;
    }
}
