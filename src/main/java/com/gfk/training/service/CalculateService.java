package com.gfk.training.service;

import com.gfk.training.domain.CalculateEnum;

/**
 * @author Rashidi Zin
 */
public class CalculateService {

    private int a;
    private int b;
    private String operation;

    public CalculateService(int a, int b, String operation) {
        this.a = a;
        this.b = b;
        this.operation = operation;
    }

    public int calculate() throws IllegalArgumentException {
        CalculateEnum operation = CalculateEnum.fromCanonicalValue(this.operation);

        if (CalculateEnum.SUBTRACT.equals(operation)) {
            return subtract();
        }
        else if (CalculateEnum.ADD.equals(operation)) {
            return add();
        }

        return -1;
    }

    private int add() {
        return a + b;
    }

    private int subtract() {
        return a - b;
    }
}
