package com.gfk.training.junit;

import java.math.BigDecimal;

/**
 * @author Rashidi Zin
 */
public class AccountService {

    private BigDecimal balance;

    public AccountService() {
        this(BigDecimal.ZERO);
    }

    public AccountService(BigDecimal balance) {

        if (balance.compareTo(BigDecimal.ZERO) < 0) {
            throw new IllegalArgumentException("Invalid amount!");
        }

        this.balance = balance;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void deposit(BigDecimal amount) {

        if (amount.compareTo(BigDecimal.ZERO) < 1) {
            throw new IllegalArgumentException("Deposit amount must be more than zero!");
        }

        this.balance = balance.add(amount);
    }

    public void withdraw(BigDecimal amount) {

        if (amount.compareTo(BigDecimal.ZERO) < 1) {
            throw new IllegalArgumentException("Invalid withdrawal amount!");
        }

        if (amount.compareTo(balance) > 0) {
            throw new IllegalArgumentException("Withdrawal amount must not be more than balance!");
        }

        if (balance.subtract(amount).compareTo(new BigDecimal(100)) < 0) {
            throw new IllegalArgumentException("Balance must be minimum of 100");
        }

        this.balance = balance.subtract(amount);
    }

    public void transfer(BigDecimal amount) {

        if (amount.compareTo(BigDecimal.ZERO) < 1) {
            throw new IllegalArgumentException("Invalid transfer amount");
        }

        if (balance.compareTo(amount) < 0) {
            throw new IllegalArgumentException("Insufficient balance to transfer");
        }

        if (balance.subtract(amount).compareTo(new BigDecimal(100)) < 0) {
            throw new IllegalArgumentException("Balance must be minimum of 100");
        }

        this.balance = balance.subtract(amount);
    }
}
