package com.gfk.training;

import com.gfk.training.service.CalculateService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author Rashidi Zin
 */
public class App {

    public static void main( String[] args ) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.print("Enter first value: ");
        int firstValue = Integer.valueOf(br.readLine());

        System.out.print("Enter second value: ");
        int secondValue = Integer.valueOf(br.readLine());

        System.out.print("Choose operation (+ or -): ");
        String operation = br.readLine();

        CalculateService calculateService = new CalculateService(firstValue, secondValue, operation);
        int result = calculateService.calculate();

        System.out.println( "Result: " + result);
    }
}
